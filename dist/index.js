'use strict';

var fs = require('fs');
var util = require('util');
var nodeHtmlParser = require('node-html-parser');
var glob = require('glob');

const asyncReadFile = util.promisify(fs.readFile);

function globber(pattern, options) {
  return new Promise((resolve, reject) => {
    glob.glob(pattern, options, (error, matches) => {
      if(error) {
        reject(error);
      }
      resolve(matches);
    });
  });
}

async function loadSprites(file) {
  const contents = await asyncReadFile(file);
  const sprites = nodeHtmlParser.parse(contents);
  const symbols = sprites.querySelectorAll('symbol');
  for(const symbol of symbols) {
    symbol.classList.add('sprite');
  }
  return sprites;
}

function spriteLoader(options = { patterns: [ ], options: { } }) {
  return {
    name: 'sprite-loader',
    async transformIndexHtml(html) {
      if(options.patterns.length <= 0) {
        throw Error('Vite Sprite Loader: no file patterns provided.');
      }
      const root = nodeHtmlParser.parse(html);
      const body = root.querySelector('body');
      body.innerHTML = `<div id='vite-sprite-loader' hidden=''></div>${body.innerHTML}`;
      const hook = root.querySelector('div#vite-sprite-loader');
      for(const pattern of options.patterns) {
        const files = await globber(pattern, options.options);
        for(const file of files) {
          const sprites = await loadSprites(file);
          hook.appendChild(sprites);
        }
      }
      return root.toString();
    }
  };
}

module.exports = spriteLoader;
