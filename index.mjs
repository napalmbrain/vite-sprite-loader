import { readFile } from 'fs';
import { promisify } from 'util';

import { parse } from 'node-html-parser';
import { glob } from 'glob';

const asyncReadFile = promisify(readFile);

function globber(pattern, options) {
  return new Promise((resolve, reject) => {
    glob(pattern, options, (error, matches) => {
      if(error) {
        reject(error);
      }
      resolve(matches);
    });
  });
}

async function loadSprites(file) {
  const contents = await asyncReadFile(file);
  const sprites = parse(contents);
  const symbols = sprites.querySelectorAll('symbol');
  for(const symbol of symbols) {
    symbol.classList.add('sprite');
  }
  return sprites;
}

export default function spriteLoader(options = { patterns: [ ], options: { } }) {
  return {
    name: 'sprite-loader',
    async transformIndexHtml(html) {
      if(options.patterns.length <= 0) {
        throw Error('Vite Sprite Loader: no file patterns provided.');
      }
      const root = parse(html);
      const body = root.querySelector('body');
      body.innerHTML = `<div id='vite-sprite-loader' hidden=''></div>${body.innerHTML}`;
      const hook = root.querySelector('div#vite-sprite-loader');
      for(const pattern of options.patterns) {
        const files = await globber(pattern, options.options);
        for(const file of files) {
          const sprites = await loadSprites(file);
          hook.appendChild(sprites);
        }
      }
      return root.toString();
    }
  };
}